package reader

type Row struct {
	Metadata map[string]string
	LTVs     [][2]float64
}

type Handler func(row Row) error
