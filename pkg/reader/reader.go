package reader

import (
	"encoding/csv"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func ParseFile(sourceFilepath string, handle Handler) error {
	fileext := filepath.Ext(sourceFilepath)

	switch fileext {
	case ".csv":
		return parseCsv(sourceFilepath, handle)
	case ".json":
		return parseJson(sourceFilepath, handle)
	default:
		return errors.New("file ext not found")
	}
}

func parseValue(value interface{}) (float64, error) {
	switch i := value.(type) {
	case float64:
		return i, nil
	case float32:
		return float64(i), nil
	case int64:
		return float64(i), nil
	case int32:
		return float64(i), nil
	case int:
		return float64(i), nil
	case uint64:
		return float64(i), nil
	case uint32:
		return float64(i), nil
	case uint:
		return float64(i), nil
	case string:
		return strconv.ParseFloat(i, 64)
	default:
		return 0, errors.New(fmt.Sprintf("error convert: %v", i))
	}
}

func parseJson(sourceFilepath string, handle Handler) error {
	jsonFile, err := os.Open(sourceFilepath)

	if err != nil {
		return err
	}

	defer jsonFile.Close()

	byteValue, err := io.ReadAll(jsonFile)

	if err != nil {
		return err
	}

	var body []map[string]interface{}

	if err := json.Unmarshal(byteValue, &body); err != nil {
		return err
	}

	for _, item := range body {
		var ltvs [][2]float64

		for k, v := range item {
			strs := strings.Split(k, "Ltv")

			if len(strs) > 1 {
				value, err := parseValue(v)

				if err != nil {
					return err
				}

				i, err := strconv.ParseFloat(strs[1], 64)

				if err != nil {
					return err
				}

				ltvs = append(ltvs, [2]float64{i, value})
			}
		}

		if err := handle(Row{
			Metadata: map[string]string{
				"Country":    item["Country"].(string),
				"CampaignId": item["CampaignId"].(string),
			},
			LTVs: ltvs,
		}); err != nil {
			return err
		}
	}

	return nil
}

func parseCsv(sourceFilepath string, handle Handler) error {
	file, err := os.Open(sourceFilepath)

	if err != nil {
		return err
	}

	defer file.Close()

	reader := csv.NewReader(file)

	//records := recordsAll[1:]

	if _, err := reader.Read(); err != nil {
		return err
	}

	for {
		record, err := reader.Read()

		if err != nil {
			if err == io.EOF {
				break
			}

			return err
		}

		campaignId := record[1]
		country := record[2]

		ltvs := record[3:]
		var ltvsValues [][2]float64

		for i, ltv := range ltvs {
			value, err := strconv.ParseFloat(ltv, 64)

			if err != nil {
				log.Println(record)
				return errors.New(fmt.Sprintf("Value %s invalid: %s", ltv, err))
			}

			if value > 0 {
				ltvsValues = append(ltvsValues, [2]float64{float64(i + 1), value})
			}
		}

		if err := handle(Row{
			Metadata: map[string]string{
				"Country":    country,
				"CampaignId": campaignId,
			},
			LTVs: ltvsValues,
		}); err != nil {
			return err
		}

	}

	return nil
}
