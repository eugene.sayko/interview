package forecast

import (
	"github.com/sajari/regression"
)

type LinearRegression struct {
}

func NewLinearRegression() LinearRegression {
	return LinearRegression{}
}

func (model LinearRegression) Predict(sources []Point, n int) (float64, error) {
	r := &regression.Regression{}

	for _, point := range sources {
		r.Train(regression.DataPoint(point.Y, []float64{point.X}))
	}

	if err := r.Run(); err != nil {
		return 0, err
	}

	value, err := r.Predict([]float64{float64(n)})

	if err != nil {
		return 0, err
	}

	return value, nil
}
