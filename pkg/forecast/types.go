package forecast

type Model string

const (
	LinearExtrapolationModel Model = "linear_extrapolation"
	LinearRegressionModel    Model = "linear_regression"
)

type Point struct {
	X, Y float64
}

type Forecaster interface {
	Predict(sources []Point, n int) (float64, error)
}
