package forecast

import (
	"errors"
)

func NewForecast(model Model) (Forecaster, error) {
	switch model {
	case LinearExtrapolationModel:
		return newLinearExtrapolation(), nil
	case LinearRegressionModel:
		return NewLinearRegression(), nil
	default:
		return nil, errors.New("model not found")
	}
}
