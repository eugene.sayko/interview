package forecast

import "errors"

func NewModel(model string) (Model, error) {
	switch model {
	case string(LinearRegressionModel), string(LinearExtrapolationModel):
		return Model(model), nil
	default:
		return "", errors.New(`must be one of "linear_regression", "linear_extrapolation"`)
	}
}
