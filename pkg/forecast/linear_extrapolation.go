package forecast

import (
	"errors"
)

type linearExtrapolation struct {
}

func newLinearExtrapolation() linearExtrapolation {
	return linearExtrapolation{}
}

func (model linearExtrapolation) Predict(sources []Point, n int) (float64, error) {
	if len(sources) < 2 {
		return 0, errors.New("too little data")
	}

	first := sources[len(sources)-2]
	second := sources[len(sources)-1]

	return first.Y + ((float64(n)-first.X)/(second.X-first.X))*(second.Y-first.Y), nil
}
