package cmd

import (
	"errors"
	"flag"
	"fmt"
	"interview/internal/calculation"
	"interview/pkg/forecast"
)

func runCalculate(model string, source string, ltvLevel int, aggregate string) error {
	aggregateValue, err := calculation.NewAggregateType(aggregate)

	if err != nil {
		return err
	}

	modelValue, err := forecast.NewModel(model)

	if err != nil {
		return err
	}

	result, err := calculation.CalculateForecast(source, modelValue, aggregateValue, ltvLevel)

	if err != nil {
		return err
	}

	for k, v := range result {
		fmt.Printf("%s: %f\n", k, v)
	}

	return nil
}

func Run() error {
	model := flag.String("model", "", "type of model forecasting (required)")
	source := flag.String("source", "", "source file path (required)")
	ltvLevel := flag.Int("ltv_level", 60, "ltv level")
	aggregate := flag.String("aggregate", "", "aggregate field (required)")

	flag.Parse()

	if (*model) == "" {
		return errors.New("model is required")
	}

	if (*source) == "" {
		return errors.New("source is required")
	}

	if (*aggregate) == "" {
		return errors.New("aggregate is required")
	}

	if err := runCalculate(*model, *source, *ltvLevel, *aggregate); err != nil {
		return err
	}

	return nil
}
