package calculation

import "interview/pkg/forecast"

type Item struct {
	Key    string
	Values []forecast.Point
}

type AggregateType string

const (
	CountryAggregateType    AggregateType = "Country"
	CampaignIdAggregateType AggregateType = "CampaignId"
)
