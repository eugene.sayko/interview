package calculation

import (
	"interview/internal/aggregator"
	"interview/pkg/forecast"
	reader2 "interview/pkg/reader"
	"slices"
)

func mean(nums []float64) (mean float64) {
	if len(nums) == 0 {
		return 0.0
	}
	for _, n := range nums {
		mean += n
	}
	return mean / float64(len(nums))
}

func movingAverage(num []float64, n int) float64 {
	if n == 0 {
		return num[0]
	}

	m := movingAverage(num, n-1)

	return m + (num[n]-m)/float64(n)
}

func medianValue(data []float64) float64 {
	dataCopy := make([]float64, len(data))
	copy(dataCopy, data)

	slices.Sort(dataCopy)

	var median float64
	l := len(dataCopy)
	if l == 0 {
		return 0
	} else if l%2 == 0 {
		median = (dataCopy[l/2-1] + dataCopy[l/2]) / 2
	} else {
		median = dataCopy[l/2]
	}

	return median
}

func CalculateForecast(sourcespath string, model forecast.Model, aggregateKey AggregateType, n int) (map[string]float64, error) {
	fc, err := forecast.NewForecast(model)

	if err != nil {
		return nil, err
	}

	agg := aggregator.New(fc, 9000, 8, n)

	agg.Run()

	if err := reader2.ParseFile(sourcespath, func(row reader2.Row) error {
		points := make([]forecast.Point, len(row.LTVs))

		for i, ltv := range row.LTVs {
			points[i] = forecast.Point{
				X: ltv[0],
				Y: ltv[1],
			}
		}

		return agg.Add(aggregator.Item{
			Key:     row.Metadata[string(aggregateKey)],
			Sources: points,
		})
	}); err != nil {
		return nil, err
	}

	aggregates, err := agg.Wait()

	if err != nil {
		return nil, err
	}

	result := map[string]float64{}

	for k, values := range aggregates {
		result[k] = mean(values)
	}

	return result, nil
}
