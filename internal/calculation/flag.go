package calculation

import (
	"errors"
)

func NewAggregateType(v string) (AggregateType, error) {
	switch v {
	case string(CountryAggregateType), string(CampaignIdAggregateType):
		return AggregateType(v), nil
	default:
		return "", errors.New(`must be one of "Country", "CampaignId"`)
	}
}
