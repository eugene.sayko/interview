package aggregator

import (
	"interview/pkg/forecast"
	"sync"
)

func New(fc forecast.Forecaster, limit int, countWorkers int, ltvLevel int) *Aggregator {
	return &Aggregator{
		ch:           make(chan Item, limit),
		ltvLevel:     ltvLevel,
		fc:           fc,
		limit:        limit,
		countWorkers: countWorkers,
		wg:           &sync.WaitGroup{},
		results:      &results{results: make([]result, 0)},
	}
}

func (aggregator *Aggregator) worker() {
	defer aggregator.wg.Done()

	for item := range aggregator.ch {
		if aggregator.err != nil {
			break
		}

		fc, err := aggregator.fc.Predict(item.Sources, aggregator.ltvLevel)

		if err != nil {
			aggregator.err = err
			break
		}

		aggregator.results.Lock()

		aggregator.results.results = append(aggregator.results.results, result{
			Key:      item.Key,
			Forecast: fc,
		})

		aggregator.results.Unlock()

	}
}

func (aggregator *Aggregator) Add(item Item) error {
	if aggregator.err == nil {
		aggregator.ch <- item
	}

	return aggregator.err
}

func (aggregator *Aggregator) Run() {
	for i := 0; i < aggregator.countWorkers; i++ {
		aggregator.wg.Add(1)
		go aggregator.worker()
	}
}

func (aggregator *Aggregator) Wait() (map[string][]float64, error) {
	close(aggregator.ch)
	aggregator.wg.Wait()

	if aggregator.err != nil {
		return nil, aggregator.err
	}

	aggregateResult := map[string][]float64{}

	for _, item := range aggregator.results.results {
		if aggregateResult[item.Key] == nil {
			aggregateResult[item.Key] = []float64{item.Forecast}
		} else {
			aggregateResult[item.Key] = append(aggregateResult[item.Key], item.Forecast)
		}
	}

	return aggregateResult, nil
}
