package aggregator

import (
	"interview/pkg/forecast"
	"sync"
)

type Aggregator struct {
	ch           chan Item
	err          error
	fc           forecast.Forecaster
	ltvLevel     int
	limit        int
	countWorkers int
	wg           *sync.WaitGroup
	results      *results
}

type Item struct {
	Key     string
	Sources []forecast.Point
}

type results struct {
	sync.Mutex
	results []result
}

type result struct {
	Key      string
	Forecast float64
}
