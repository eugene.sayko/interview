package main

import (
	"fmt"
	"interview/cmd"
	"log"
	_ "net/http/pprof"
	"time"
)

func main() {
	fmt.Println("start forecast")

	t := time.Now()

	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}

	fmt.Println("completed: ", time.Since(t))
}
