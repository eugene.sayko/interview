# Параметры
- ## **model** (required) - linear_extrapolation, linear_regression
- ## **source** (required) - путь к источнику данных
- ## **aggregate** (required) - ключ для аггрегации данных
- ## **ltv_level** (default: 60) - к какому дню спрогнозировать ltv пользователя

  ## Пример запуска
    #### go run main.go --source=./test_data.csv --aggregate=CampaignId --model=linear_extrapolation --ltv_level=70

# Основные компоненты

- ## pkg/forecast
    ### Моделям нужно реализовать метод **Predict** который на вход принимает список точек(x,y), где x - это день, а y значение в этом дне
- ## pkg/reader
    ### принимает на вход путь к файлу с данными и коллбэк в который передается элемент из переданных данных, если обработчик возвращает ошибку, то reader прекращает свою работу
- ## internal/aggregator
    ### запускает n-ое количество воркеров и принимает данные для которых нужно сделать прогноз, например: {key:"test", sources:[]Point}, после завершения работы возвращает map со списком прогнозов для каждого key
- ## internal/calculation
    ### инициализирует модель для прогноза, запускает считывание данных и пробрасывает считанные данные в **aggregator**

# Внедренные модели

- ## линейная экстраполяция
    ### Основной источник https://en.wikipedia.org/wiki/Extrapolation
- ## линейная регрессия
    ### Основной источник https://aws.amazon.com/what-is/linear-regression/?nc1=h_ls

    Пробовал добавлять например ARIMA, но слишком мало данных для такой модели